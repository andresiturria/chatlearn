package principal;

public class Mensaje {
    private String emisor, receptor, contenido;

    public Mensaje(){ super();}

    public String getEmisor() {
        return this.emisor;
    }

    public void setEmisor(String emisor) { this.emisor = emisor; }

    public String getReceptor() {
        return this.receptor;
    }

    public void setReceptor(String receptor) {
        this.receptor = receptor;
    }

    public String getContenido() {
        return this.contenido;
    }

    public void setContenido(String contenido) {this.contenido = contenido;
    }
}
